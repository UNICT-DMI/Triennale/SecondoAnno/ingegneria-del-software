

import java.util.List;

public interface Spada
{
	public String getNome();
	public int getDanno();
	public List<String> getEffetti();
}
