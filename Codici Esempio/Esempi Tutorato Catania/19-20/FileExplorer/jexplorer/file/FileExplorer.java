package jexplorer.file;

import java.io.File;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;

import java.io.PrintStream;

import java.util.HashMap;
import java.util.WeakHashMap;

public class FileExplorer {
    protected File root;
    protected WeakHashMap<String, String> cache;

    public FileExplorer(String rootPath) {
        root = new File(rootPath);

        cache = new WeakHashMap<>();
    }

    public void explore(PrintStream stream) throws IOException {
        scan(root, stream);
    }

    protected void scan(File file, PrintStream stream) throws IOException {
        if(file.isFile()) {
            stream.println(file + " is a regular file, reading content...");

            printFileContent(file, stream);
            return;
        }

        if(file.isDirectory()) {
            stream.println(file + " is a directory, exploring recursively...");

            File[] files = file.listFiles();
            for(File child : files) {
                scan(child, stream);
            }
        }
    }

    protected void printFileContent(File f, PrintStream stream) throws IOException{
        BufferedReader reader = new BufferedReader(new FileReader(f));

        String fileKey = f.getAbsolutePath();
        String content = null;

        if(cache.containsKey(fileKey)) {
            stream.println("File content is cached");

            content = cache.get(fileKey);
        } else {
            stream.println("File content is not cached, accessing the filesystem...");

            StringBuilder builder = new StringBuilder();

            while(reader.ready()) {
                String line = reader.readLine();

                builder.append(line);
            }

            content = builder.toString();

            stream.println("Saving content in cache...");

            cache.put(fileKey, content);
        }

        stream.println(content);
    }
}