import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;

import java.util.Arrays;

public class FileExamples {
    public static void main(String[] args) {
        if(args.length != 1) {
            System.err.println("Parametri errati");
            System.exit(1);
        }

        System.out.println("Caricamento directory...");
        File d = new File(args[0]);

        if(!d.isDirectory()) {
            System.err.println("Il percorso specificato non è una directory");
            System.exit(2);
        }

        for(File f : d.listFiles()) {
            if(f.isDirectory()) {
                System.out.println(f + " è una directory.");
                continue;
            }

            System.out.println(f + " è un file, ne stampo il contenuto...");
            System.out.println("---\n");

            long startTime = System.currentTimeMillis();

            try {
                printFileContentWithBufferedReader(f);
            } catch(IOException e) {
                System.out.println("Impossibile leggere il file " + f + " - " + e);
            }

            System.out.println("\n---");

            long endTime = System.currentTimeMillis();

            System.out.println("DEBUG: Tempo di elaborazione: " + (endTime - startTime) + "ms");
        }

        System.out.println("Fine");
    }

    protected static void printFileContentWithLineNumbers(File f) throws IOException{
        LineNumberReader reader = new LineNumberReader(new FileReader(f));

        // reader.lines().forEach((line) -> {
        //     System.out.println(line);
        // });

        while(reader.ready()) {
            String line = reader.readLine();
            int lineNumber = reader.getLineNumber();

            System.out.println(lineNumber + ": " + line);
        }
    }

    protected static void printFileContentWithBufferedReader(File f) throws IOException{
        BufferedReader reader = new BufferedReader(new FileReader(f));

        while(reader.ready()) {
            String line = reader.readLine();

            System.out.println(line);
        }
    }

    protected static void printFileContentWithCharBuffer(File f) throws IOException{
        FileReader reader = new FileReader(f);

        char[] buffer = new char[2048];

        while(reader.ready()) {
            int chars_read = reader.read(buffer);

            if(chars_read < buffer.length) {
                for(int i = chars_read; i < buffer.length; ++i) {
                    buffer[i] = 0;
                }
            }

            if(chars_read > 0) {
                System.out.print(buffer);
            }
        }
    }
}