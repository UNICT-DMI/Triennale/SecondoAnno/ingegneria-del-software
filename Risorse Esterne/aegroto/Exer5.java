/*
    Dato uno stream delle prime k potenze di due, ritornare la somma delle 
    sue componenti la quale ultima cifra è 8. Il programma deve funzionare
    per ogni intero k
*/    

import java.util.ArrayList;
import java.util.stream.Stream;
import java.util.Optional;

public class Exer5 {
    public static void main(String[] args) throws Exception {
        final int k = 20;

        System.out.print("Adding: ");

        Optional<Integer> result = Stream.iterate(2, x -> x * 2)
                           .limit(k)
                           .filter(x -> x % 10 == 8)
                           .peek(x -> System.out.printf("%d ", x))
                           .reduce(Integer::sum);

        System.out.print("\n");

        if(result.isPresent()) {
            System.out.printf("Result is: %d\n",result.get());
        } else {
            throw new Exception("Something went wrong, check your code.");
        }
    }
}